﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSliding : MonoBehaviour
{
    public RectTransform rectContentPanel;
    public Button btnMusic;
    public Button btnSettings;
    public float speed = 15;
    private bool showLeftScreen = true;
    private bool isMove = false;
    

    private float screenWidth = 0;

    // Start is called before the first frame update
    void Start()
    {
        screenWidth = Camera.main.orthographicSize * (2f * Screen.width / Screen.height);
        Debug.Log("ScreenWidth: " + screenWidth);
        btnMusic.onClick.AddListener(ShowListMusic);
        btnSettings.onClick.AddListener(ShowSettings);
    }

    // Update is called once per frame
    void Update()
    {
        updateContentPage();
    }

    private void ShowListMusic()
    {
        showLeftScreen = true;
        isMove = true;
    }

    private void ShowSettings()
    {
        showLeftScreen = false;
        isMove = true;
    }

    private void updateContentPage()
    {
        if (!isMove)
            return;

        float xDistance = Time.deltaTime * speed;
        if (showLeftScreen)
        {
            if (rectContentPanel.transform.position.x + xDistance > 0)
            {
                isMove = false;
                xDistance = -rectContentPanel.transform.position.x;
            }
            rectContentPanel.transform.Translate(xDistance, 0, 0);
        } else
        {
            if (rectContentPanel.transform.position.x - xDistance < -screenWidth)
            {
                isMove = false;
                xDistance = rectContentPanel.transform.position.x + screenWidth;
            }
            rectContentPanel.transform.Translate(-xDistance, 0, 0);
        }
    }
}
